﻿using System.Text.Json.Serialization;

namespace CSVSerializer;

internal class F
{
    [JsonInclude]
    private int i1, i2, i3, i4, i5; 
    public bool I6 { get; set; }
    
    public static F Get() => new() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5, I6 = true };

    public override string ToString()
    {
        return $"i1 = {i1}, i2 = {i2}, i3 = {i3}, i4 = {i4}, i5 = {i5}, I6 = {I6}";
    }
}
