﻿/*Рефлексия и её применение

Написать свой класс-сериализатор данных любого типа в формат CSV, сравнение его быстродействия 
с типовыми механизмами серализации.

1    Написать сериализацию свойств или полей класса в строку
2    Проверить на классе: class F { int i1, i2, i3, i4, i5; Get() => new F(){ i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 }; }
3    Замерить время до и после вызова функции (для большей точности можно сериализацию сделать в цикле 100-100000 раз)
4    Вывести в консоль полученную строку и разницу времен
5    Отправить в чат полученное время с указанием среды разработки и количества итераций
6    Замерить время еще раз и вывести в консоль сколько потребовалось времени на вывод текста в консоль
7    Провести сериализацию с помощью каких-нибудь стандартных механизмов (например в JSON)
8    И тоже посчитать время и прислать результат сравнения
9    Написать десериализацию/загрузку данных из строки (ini/csv-файла) в экземпляр любого класса
10    Замерить время на десериализацию
11    Общий результат прислать в чат с преподавателем в системе в таком виде:
    Сериализуемый класс: class F { int i1, i2, i3, i4, i5;}
    код сериализации-десериализации: ...
    количество замеров: 1000 итераций
    мой рефлекшен:
    Время на сериализацию = 100 мс
    Время на десериализацию = 100 мс
    стандартный механизм (NewtonsoftJson):
    Время на сериализацию = 100 мс
    Время на десериализацию = 100 мс
*/

using System.Diagnostics;
using System.Text.Json;

namespace CSVSerializer;

internal class Program
{
    static void Main()
    {
        const int ITERS = 100_000;
        F fObj = F.Get();
        CSVSerializer csvSerializer = new();
        string csv = null!;
        string json = null!;
        var jsonOptions = new JsonSerializerOptions
        {
            IncludeFields = true,
            WriteIndented = true,
        };

        Console.WriteLine("Windows. Visual Studio 2022\n");

        Profile("CSV serialization", ITERS, () => csv = csvSerializer.Serialize(fObj));
        Profile("JSON serialization", ITERS, () => json = JsonSerializer.Serialize(fObj, jsonOptions));
        Console.WriteLine($"\nResulting CSV:\n{csv}");
        Console.WriteLine($"\nResulting JSON:\n{json}\n");

        F? fObjFromCsv = null;
        F? fObjFromJson = null;
        Profile("CSV Deserialization", ITERS, () => fObjFromCsv = csvSerializer.Deserialize<F>(csv));
        Profile("JSON Deserialization", ITERS, () => fObjFromJson = JsonSerializer.Deserialize<F>(json));

        var watch = Stopwatch.StartNew();
        Console.WriteLine("\nInitial F object:");
        Console.WriteLine(fObj);
        Console.WriteLine("F object deserialized from CSV:");
        Console.WriteLine(fObjFromCsv);
        Console.WriteLine("F object deserialized from Json:");
        Console.WriteLine(fObjFromJson);
        watch.Stop();
        Console.WriteLine($"\nTime to WriteLine F objects: {watch.ElapsedTicks} ticks");
        Console.ReadKey();
    }

    // модифицированный метод из StackOverflow для замеров
    static void Profile(string description, int iterations, Action action)
    {
        // clean up
        GC.Collect();
        GC.WaitForPendingFinalizers();
        GC.Collect();

        // warm up
        action();

        var loops = iterations / 10;
        var watch = Stopwatch.StartNew();
        for (int i = 0; i < loops; i++)
        {
            action();
            action();
            action();
            action();
            action();
            action();
            action();
            action();
            action();
            action();
        }
        watch.Stop();
        Console.WriteLine(description);
        Console.WriteLine("{0:0.00} ms ({1:N0} ticks) (over {2:N0} iterations), {3:N0} ops/milliseconds",
            watch.ElapsedMilliseconds, watch.ElapsedTicks, iterations,
            (double)iterations / watch.ElapsedMilliseconds);
    }
}
