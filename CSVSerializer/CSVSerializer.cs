﻿using System.Reflection;
using System.Text;

namespace CSVSerializer;

internal class CSVSerializer
{
    public string Delimiter { get; private set; }

    public CSVSerializer(string delimiter = ",") => Delimiter = delimiter;

    public string Serialize<T>(T obj)
    {
        ArgumentNullException.ThrowIfNull(obj);
        
        StringBuilder sbHeaders = new();
        StringBuilder sbValues = new();
        Type type = obj.GetType();

        FieldInfo[] fields = type.GetFields(
            BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
        foreach (FieldInfo field in fields)
        {
            sbHeaders.Append(field.Name + Delimiter);
            sbValues.Append(field.GetValue(obj) + Delimiter);
        }

        PropertyInfo[] properties = type.GetProperties(
            BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
        foreach (PropertyInfo property in properties)
        {
            sbHeaders.Append(property.Name + Delimiter);
            sbValues.Append(property.GetValue(obj) + Delimiter);
        }

        sbHeaders.Remove(sbHeaders.Length - Delimiter.Length, Delimiter.Length);
        sbValues.Remove(sbValues.Length - Delimiter.Length, Delimiter.Length);

        return sbHeaders.ToString() + "\r\n" + sbValues.ToString();
    }

    public T Deserialize<T>(string csv) where T : new()
    {
        ArgumentNullException.ThrowIfNull(csv);

        string[] headersAndValues = csv.Split("\r\n");
        if (headersAndValues.Length > 2)
        {
            throw new NotImplementedException("Multiobject CSVs are not supported.");
        }
        else if (headersAndValues.Length < 2)
        {
            throw new ArgumentException("CSV is corrupted or format is unsupported.");
        }
        string[] dataHeaders = headersAndValues[0].Split(Delimiter);
        string[] dataValues = headersAndValues[1].Split(Delimiter);
        if (dataHeaders.Length != dataValues.Length)
        {
            throw new FormatException("Can't match headers and values. Wrong CSV format?");
        }

        T obj = new();
        Type type = obj.GetType();
        for (int i = 0; i < dataHeaders.Length; i++)
        {
            FieldInfo? field = type.GetField(dataHeaders[i],
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
            if(field is not null)
            {
                Type fieldType = field.FieldType;
                object value = Convert.ChangeType(dataValues[i], fieldType);
                field.SetValue(obj, value);
            }
            else
            {
                PropertyInfo? property = type.GetProperty(dataHeaders[i],
                    BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
                if (property is not null)
                {
                    Type propertyType = property.PropertyType;
                    object value = Convert.ChangeType(dataValues[i], propertyType);
                    property.SetValue(obj, value);
                }
                else
                {
                    throw new Exception($"Can't set one or more members. {type.Name} has been changed?");
                }
            }
        }
        return obj;
    }
}
